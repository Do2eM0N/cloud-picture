# 1.数据的基本类型

​	（1）整型int

​	（2）字符型char

​	（3）实型（浮点型）

​				单精度实型float

​					双精度实型double

精确到第几位？

例如%。3f，就是小数点后保留三位

# 2.输入与输出  scanf printf

每一个scanf函数调用都紧跟在一个printf函数调用的后面，这样做可以提示用户何时输入，以及输入什么。

##         （1）输入函数scanf

​							例：num1把那个值放在那个地址处

清空输入缓冲区

如果在scanf("%d,%d,%d",&a,&b,&c)

这双引号里面有逗号，那么在控制台之内输入的时候就要以逗号隔开

双引号中间的叫做格式化字符串，&叫做取地址符。



## 			（2）输出函数printf

printf("",)格式化字符串  输出列表

### 1.使用Printf函数输出八进制和十六进制数

```c
转换符号“%o”、“%X”
```



### 2.域宽

指的是存放输出数据的宽度

# 3.Main函数

(1)主函数程序的如空有且只有一个

（2）int main (mian前面的int表示main函数调用返回一个整型值)

（3）void main 已经过时

（4）main放在哪里都行，都先执行他

# 4.打印的类型

## （1）%d 

（十进制有符号整数）

例如%-6d——输出的整型占6个字符宽

-号为左对齐符号，默认为向右对齐



%u 十进制无符号整数
%f 浮点数
%s 字符串

%c 单个字符
%p 指针的值
%e 指数形式的浮点数
%x, %X 无符号以十六进制表示的整数
%0 无符号以八进制表示的整数
%g 自动选择合适的表示法

# 5.计算所占字节数量

计算元素个数

sizeof(arr)/sizeof(arr[0])

sizeof



# 6.计算机中的单位

(1) bit 比特位

(2) byte字节

(3) kb

(4) mb

(5) gb

(6) tb

(7) pb

# 7.变量

（1）定义变量的类型：类型 变量名 赋值

（2）{}大括号

（3）定义在{}之外的叫做全局变量

  (4)全局变量和局部变量可以同时存在，并且局部变量优先

（5）局部变量和全局变量的名字最好不要一样，容易产生bug

  (6)局部变量只能在他所在的那个局部使用

（7）c语言语法规定，变量要定义在当前代码块的前面

# 8.常量

## （1）字面常量

3： 100： 3.14：

## （2）const修饰的常变量

​     	（const常属性）

```C
const int num = 4; 
```



```c
const int n = 10; 

//n是变量 但是又有常属性,所以我们称n是常变量
```

##     (3)#define定义的标识符常量

## （4）枚举常量

```C
//枚举---一一列举
//性别：男 女 保密
//三原色：红黄蓝
```

枚举关键字---enum

```c
enum Sex
{
    MALE,
    FEMALE,
    SECRET
        //这三个叫做枚举常量
}；
int main()
{
    printf("%d\n",MALE);
    printf("%d\n",FEMALE);
    printf("%d\n",SECRET);
	return 0;
}

```

# 9.字符串

例子：名字 身份证号...

==不能用来比较两个字符串是否相等，应该用一个库函数-strcmp

（1）就是双引号引出的东西

```c
int main()
{
    "abcdef";
    "hellow sb"
    "";//空字符串
}
```



```c
int main()
{
    char arr1[] = "abc";
    //数组
    printf("%s\n",arr1);
    return 0;
}
```

(2)字符串的结束标志是一个\0的转义字符，在计算字符串长度的时候\0是结束标志，不算作字符串的内容 。

```c
int main()
{
    //数据在计算机存储的时候，存储的是2进制
    //#av$
    //a - 97 
    //A -65
    //  
    //ascii 编码
    //ascii 码值
    char arr[] = "ABC" ； //数组
     //"ABC" -- 'A' 'B' 'C' '\0' -- '\0'
       //字符串的结束标志
     char arr2[] = {'A' ,'B', 'C', '\0'};
    //'\0' - 0
    //'A' - 97
    // 'A' 'B' 'C'
    printf("%s\n",arr1);
    printf("%s\n",arr2);
    
    return 0;
    
}
```

# 10.计算字符串长度

strlen()

计算字符串长度时\0是结尾的标志，不算做字符数量

**只有从前往后数的时候碰到\0才会停止**

# 11.转义字符

（转变原来的意思）



​	    (1).\n 换行

​		(2).\t 水平制表符

​					(类似于Tab)

​		（3）.\? 

​					（在连续书写多个问号的时候，防止他们被解析成三字母词）

​						(在c语言早期的时候有个东西叫做三字母词---？？+)

​		（4）.\+任意带有转义字符的字母，使其不被解析为转义字符

​		（5）\转义任意，例：'/'  转义两个单引号中后面的那个，使其打印出来的就是一个

```c
#include <stdio.h>
#include <string.h>
int main（）
{
    printf("%d\n",strlen("c:\test\32\test.c));
    return 0;
}
                         //此输出结果为13
```

（6）\ddd   ddd表示一道三个八进制数字

例子：上框中 32作为8进制代表的那个十进制数字，作为ascii码值，对应的字符

​       32=====》10进制 26======》作为ASCII码值代表的数

（7）\xdd   dd表示2个16进制的数字

# 12.库函数

```c
//(1)printf
 #include <stdio.h>
//(2)计算字符串长度
  #include <string.h>
//(3)引入数学公式
 #include <math.h>
//(4)

//(5)

//(6)

//
```

# 13.注释

```c
//(从c++引进的)
/*  */  (C原本的注释，开始 只要在碰到*/就会停止)
```

# 14. if   while语句

# 15.函数举例

例

```c
int a = 100；
int b = 200;
sum = Add(a, b) 
    //f(x) = 2*x+1
    //f(x,y) = x+y
```

 

```c
int Add (int x, int y)
{
    int z = x+y;
    return z;
}
int main()
{
 int num1 = 10;
 int num2 = 20 ;
 int sum = 0;
 int a = 100;
 int b = 200;
 sum = Add(num1,num2)
 sum = Add(a,b) 
 printf("SUM = %d\n" , sum)
     return 0;
}
```

# 16.自定义函数

# 17.数组

**一组相同元素的集合**

数组的大小要拿常量来指定

```c
int main()
{
	//int a = 1;
   // int b = 2;
    //int c = 3;
    //int 4 = 4;
    
    
    int arr[10] = {1,2,3,4,5,6,7,8,9,10};
    //定义一个存放10个整数数字的数组
    //char ch[20];
   // float arr2[5];
    return 0;
  
```

(1)下标

语法规定 数组的下标从0开始

![image-20201121115222649](https://i.loli.net/2020/11/29/2CmWdisDt1Hxe6p.png)

```c

printf("%d\n",arr[下标]);
//直接访问对应数组的元素

```

```c
//数组中
int arr[] = {1,2,3,4,,5,6};
//这里的int指的是数组中各个元素的类型，并不是数组的类型，
//数组的类型较为复杂
//求数组中的元素个数
printf("%d\n",sizeof(arr) / sizeof(arr[0])); 
```



# 18.操作符

## （1）算数操作符

## （2）移位操作符

移的位是二进制位

左移<<                 右移>> 

```c
int main ()
{
    int a = 1;
    //整型1站四个字节 ——32个bit位
    //000000000000000000000000000000001
    //四个字节放的就是这个二进制序列 
    //左边丢弃右边补零
    
    
    
}
```

![image-20201121154224594](https://i.loli.net/2020/11/29/H9MjELVF7pdbe13.png)

## （3）位操作符

这个位还是二进制位

&按位与

|按位或

^按位异或

```c
//按位与
//--对应的二进制要与（并且）一下 
//c语言中0为假，非0就是真
int main()
{
    int a = 3; //011
    int b = 5;	//101
    int c = a&b; //001  
    //只有两个都为1按位与出来才是1
    printf("%d\n",c);
}
输出结果为 //1(1)  (换为十进制)
```

```c
//按位或
int main()
{
    int a = 3;  //011
    int b = 5;	//101
    int c = a|b; //111
    //只要有一个不是0按位或出来就是1
    printf("%d\n",c);
}
输出结果为//7
```



```c
//按位异或
//计算规律 对应的二进制位相同则为0
//         对应的二进制位相异则为1 
int main()
{
    int a = 3;  //011
    int b = 5;	//101
    int c = a|b; //110
    
    printf("%d\n",c);
}
输出结果为//6
```



## （4）赋值操作符

​      [1]

```c
int main()
{		
    int a =10;
    a = 20;//赋值 // ==判断相等
 	//a = a+10;  (给a加10在赋给a)等价于 
    //a += 10;
    例子
      //a = a - 20;
        a -= 20 ；
      //a = a & 2;
        a &= 2;
    return 0;  
    //符合赋值符
    //+= -= *= ......
}
```

## （5）单目操作符

  	双目操作符

​	三目操作符

```c
int main()
{
  	int a  = 10;
    int b = 10;
    a + b;//+双目操作符
    return 0;
    
    ！---逻辑反操作
    int a = 10;
    //在c语言中我们表示真假
    //0 表示假 一切非0 表示真 
    printf("%d\n"!a )
        //为真 固定输出为一个数字1
    
}
```

​    sizeof 计算的是变量/类型所占空间的大小

单位是字节

![image-20201121162503188](https://i.loli.net/2020/11/29/yxUpEYLumiVh4HN.png)

sizeof  计算int不可省略括号 计算a可以省略括号



~对一个数的二进制进行按位取反

```c

#include <stdio.h>
int main()
{	
    int a = 0;//四个字节 32bit
    int b = ~a;
    //按位（二进制位）取反
    //1010  变成
    //0101	 
    printf("%d\n",b);
 	return 0;    
}
//原码 反码 补码
//负数在内存中存储的时候，存储的是二进制的补码
//使用的打印的是这个数的原码
//先补码-1得到反码 ，反码按位取反变成原码
```





-- ++

```c
//-- ++ （--++的值都是1 ）
#include <stdio.h>
int main()
{
    int a = 10;
    int b = a++;//后置++,先使用,再++
    // int b = ++a;前置++,先++，再使用
    //后置--  先使用 再--
    printf("a = %d\n b = %d\n", a ,b);
    return 0 ;
}
```



```C
// *
```

```c
//(类型)---强制类型转换 
#include <stido.h>
int main()
{	
    int a = (int) 3.14;//我要把3.14强制转换 3.14是double类型的
    				//这里让他强制转化为int型的
    return 0;
    
}
```





## （6）关系操作符

比较大小 看关系是什么  

大于等于>=

不等于！=

## （7）逻辑操作符  

```c
//&&逻辑与 必须两个都是非0的 一0一非0输出也为0
#include <stido.h>
int main()
{
    //真 -非0
    //假 - 0
    // 
    int a = 3;
    int b = 5;
    int c = a&&b;
 	printf("c = &d\n",c);
    return 0;
}
//
```



```c
//||逻辑或 a和b里有一个为真就行  
//他俩都为假的时候才为假 都是真的那都是真的
#include <stdio.h>
int main()
{	
    int a = 10;
    int b = 0;
    int c =  a||b;
   
    
    return 0;
}
```

## （8）条件操作符（三目操作符）

比较抽象

exp1? exp2 : exp3;

```c
//    exp1? exp2 : exp3;
#inculde <stdio.h>
int main()
{	
    int a = 100;
    int b = 20;
    int  max = 0;//存储
   
    max = (a > b ? a : b )
        //如果a>b这个表达式的结果为真
   //则max 为a  不管如何max都放的是ab的等较大值
    if (a > b)
        max = a ;
    else 
        max = b ;
    
    
    
    return 0;
}
```

## （9）逗号表达式

epx1,exp2,exp3....expn

用逗号隔开表达式

## （10）下标引用操作符

```c
#include <stdio.h>
//数组中
int main()
{	
    int arr[10] = { 0 };
    arr[4] ;//[]下标引用操作符
    return 0;
}
```



## （11）函数调用操作符

```c
#include <stdio.h>
int Add( int x, int y)
{	
    int z =0;
    z = x + y;
    return z;
    
}
int main()
{	
    int a = 10;
    int b = 20;
   	int sum =  ADD(a,b); //()-函数调用操作符
    
    return  0;

}
```





# （12）解引用操作符

（13）

（14）

# 19.复制基础框架

```C
#include <stdio.h>
int main()
{	
   
}
```





# 20.综合练习

## （1）求两个数的较大值

```c
                   
```

## (2)定义一个函数来求两个数的较大值



```c
#include <stdio.h>
int Max (int x, int y) //接收a b
{     //大括号为函数体
    if ( x > y )
        return x;
    else 
        return y;
}
int main()
{ 
    int a = 10;
    int b = 20;
    int max = 0;
    max = Max(a,b);
    //定义一个名为Max的函数
    printf("max = %d\n",max);
    
    return 0 ;
    
}
```

#  21.     原码、  补码、反码

```c
只要是整数 内存中存储的都是二机制的补码
    对于//正数来说，他的原码补码反码相同
   		//负数 存的是补码
    	原码是直接按照正方写出的二进制数列
    	反码从原码的符号位不变其他位取反得到
    	补码 反码+1
    
```



#    ![image-20201122213736103](https://i.loli.net/2020/11/29/sPKAugpDYBOeoQc.png)

# 22.常见关键字

关键字不能与变量名冲突

关键字不能自己创建

关键字不能做变量名

## （1）static

static可以修饰局部变量

可以修饰全局变量

可以修饰函数

-2![image-20201122214135006](https://i.loli.net/2020/11/29/h6HuSskTFBWyRZI.png)

![image-20201130164931937](C笔记.assets/image-20201130164931937.png)

```c
#include <stdio.h>
int main()
{	
    int a = 10;//局部变量-自动变量（省略auto）
    
    return 0;
}
```

```c
//register 
#include <stdio.h>
int main()
{	
   // register int a =10;
    //建议把a定义成寄存器变量
    int a = 10;
    a = -2;
    int 定义的变量是有符号的
        (signed) int ;
    unsigned int num = 0;
    无符号为没有正负
    return 0;
}
```

```C
//
#include <stdio.h>
int main()
{	
   //struce -结构体关键字
}
```

```c
#include <stdio.h>
 int main
           //typedef-类型定义-类型重定义
     typedef unsigned int u_int
```

```C
//static-用来修饰局部变量
//修饰局部变量时局部变量的生命周期变长
#include <stdio.h>
void test()
{
    static int a = 1; //a为一个静态的局部变量
    a++;
    printf("a = %d\n",a);
}
//出这个函数a不销毁了
int main()
{	
    int i =0;
    while (i<5)
    {
        test();
        i++;  
    }
    return 0;
}
```

![image-20201123092942576](https://i.loli.net/2020/11/29/qgJ91dckfthulMo.png)

```c
//static修饰全局变量
//改变了变量的作用域 
//让静态的全局变量只能在自己所在的原文件内部使用
//出了原文件就无法使用了
#include <stdio.h>
int main()
{	
    //extern -声明外部符号的
    extern int g_val ;
    printf("g_val = %d\n", g_val);
    return  0;
}
```

```c
//static修饰函数也是改变了函数的作用域
//这种说法不准确
//正确说法是
//static修饰函数 改变了函数的链接属性
//外部链接属性==>内部链接属性
#include <stdio.h>
//在外部 static  int Add(int x, int  y)
//        z = x + y;
//	     return z;
声明外部函数
    extern int Add(int,int);
int main()
{	
    int a = 10;
    int b = 20;
    int sum = 	Add(a,b);
    printf("sum = %d\n")
    return 0;
}
```

# 23.#define定义常量和宏

预处理指令，不是关键字

```c
#include <stdio.h>
//#define定义的标识符常量
//#define MAX 100

//#define可以定义宏-带参数

//函数的实现
int Max(int x,int y)
{
	if(x > y)
  		return x;
    else
         return y;
}
//宏的定义方式
#define MAX(X,Y) X>Y?X:Y
int main()
{	
    //int a = MAX；
   	int a = 10;
    int b = 20;
    //函数
    int max = Max(a,b);
    pritnf("max =%d\n",max);
    //宏的方式
    max = MAX(a,b);
    //max = (a>b?a:b);
    printf("max = %d\n",max);
    return 0;
```

# 24.指针

指针——是个变量 用来存放地址

每个小格子的编号——地址

（1）如何产生地址

​		32位 ——有三十二跟地址线/数据线

​		正负电10之分

​	电信号转化为为数字信号



%p用来打印地址 

​           

```c
#include <stdio.h>
int main()
{	
    int a = 10;//给a申请4个字节
    //只要是个值就可以存起来
     int* p = &a;
    //p现在是一个指针变量 
    //p的类型是int*
    //p里面存的是a的地址
    //有一种变量是用来存放地址的——指针变量
    //&a;//取地址
  //  printf("%p\n",&a);
 //  printf("%p\n,p");
    //*p,这颗*就是解引用操作符/间接访问操作符
    *p = 20;
    printf("a = dp\n",a);
    return 0;
}
```

​            

```c
int a = 10;创建变量a a的地址中放了10
int *p = &a; 创建p变量存a的地址
*p = 20;对p里面存的地址进行操作，通过p找到a
    	*p就是a,把a的10改成了20
```

​               

例子

//地址就是二进制序列

```c
#include <stdio.h>
int main()
{	
    char ch = 'w';
    char*pc = &ch;
    *pc = 'a';
    printf("%c\n",ch);
        //打印字符%c
	return 0;
}
 
```

​                                                

```c
//指针大小32位平台4个字节，64 位平台8个字节
#include <stdio.h>
int main()
{	
    char ch = "w";
    char*pc = &ch;
    printf("%d\n",sizeof(pc));
    
    return 0;
}
```



```c
#include <stdio.h>
int main()
{	
    
    return 0;
    
}
```



```

```





# 25.结构体

如何描述复杂对向——结构体

自己创造出来的一种类型

struct——结构体关键字

定义===》初始化

```c


#include <stdio.h>
//改名字的库函数
#include <string.h>

//创建一个结构体类型
struct book
{
    //数的类型
    char name[20];//c语言程序设计
    short price;
    
};
//分号结束类型定义
int main()
{	
   //利用结构体类型创建一个该类型的结构体变量出来
    struct book b1 = {"c语言程序设计"，55};
    struct book*pb = &b1;
    
    
    //利用pb打印一下书名和价格
    printf("%s\n", pb->name);
    printf("%d\n", pb->price);
    //.操作符应用于结构体变.成员
   //->结构体指针 ->成员
    
    
   /*intf("%s\n",(*pb).name);
	printf("%s\n",(*pb).price);*/
    
    
   // printf("书名：%s\n",b1.name);
    //printf("价格：%d\n",b1.price);
    b1.price = 15;//改变价格 price为变量
    		//name不可这样改
    //strcpy字符串拷贝 库函数
    //改名字 stycpy关于字符串操作
    stycpy(b1.name,"C++")
   	printf("修改后的价格：%d\n",b1.price);
    
    return 0;
}
```

# 26.分支语句和循环语句

结构 

顺序结构 选择结构 循环结构

什么是语句：用分号隔开的就是，有一个分号算一个，直写一个分号也算一条语句——空语句，

一对大括号就是一个代码块

注意if和else的匹配

else和离他最近的if相匹配



## （1）分支语句（选择结构）

### if语句

1.后面加大括号{}可以输入多条语句，否则只能输入一跳语句

2.if语句中0表示假，非0表示真

3.if语句是一种分支语句可以实现单分支，也可以实现多分支

 4.else与他最近的（未匹配的if）相配对

```c
//单分支if
#include <stdio.h>
int main()
{	
    int age = 10;
    if(age<18);
    	printf("未成年\n");
    return 0;
}
```

```C
//if else
#include <stdio.h>
int main()
{	
    int age = 10;
    if(age<18);
    	printf("未成年\n");
    else
        print("成年\n");
    return 0;
}
```

```c
//
#include <stdio.h>
int main()
{	
    int age = 10;
    if(age<18);
    	printf("未成年\n");
    else if(age>=18 &&age<28)
        printf("青年\n");
    else if(age>=28 &&age<50)
        printf("壮年\n");
    else if(age>=50 &&age<90)
        printf("老年\n");
    else  
        printf("老不死")；
    return 0;
}
```



```C
#include <stdio.h>
int main()
{	
    int age = 10;
    if(age<18);
    	printf("未成年\n");
    else
    {
    	if(age>=18 &&age<28)
        printf("青年\n");
    else if(age>=28 &&age<50)
        printf("壮年\n");
    else if(age>=50 &&age<90)
        printf("老年\n");
    else  
        printf("老不死")；
    }
    return 0;
}
//同上
```

```c
// =赋值 ==判断相等
int main()
{	
    int num = 1;
    if(5 == num) //这样写 出现错误可以立即显示 便于发现与改正
    {
        printf("sb\n");
    }
    return 0;
}
```

### 练习

```c
判断一个数是否为奇数 输入1-100之间的奇数
#include<stdio.h>
#include<string.h>
 //我的错误示范
/* int main
{	
    int a = 0;
    printf("请输入一个数字");
    scanf("%d\n",&);
    if(a%2 == 0)
        printf("不是");
   	else
        printf("是");
    return 0;
}
```



```c

//正确
//方法
#include <stdio.h>
int main()
{	
    int i = 1;
    while(i<=100)
    {
        if(i%2 == 1)
            printf("%d\n",i);
        i++;
    }
    return 0;
}
//方法2 
#include <stdio.h>
int main()
{	
    int i = 1;
    while(i<=100)
    {	
        if(i%2 !=0)
        	printf("%d", i);
        i++;
    }
    return 0;
}
```



### switch语句

分支语句 

switch（整型表达式）

搭配break实现分支

产生的结果必须为整型

case整型常量表达式 case后面必需为整型常量表达式，

不要求顺序

default字句可以放在任意位置





```c
#include <stdio.h>
//case入口 braek跳出语句
int main()
{
	int day = 0;
	scanf("%d", &day);
	switch (day)
	{
	case 1:
		printf("星期一\n");
         break;
	case 2:
		printf("星期二\n");
         break;
	case 3:
		printf("星期三\n");
         break;
	case 4:
		printf("星期四\n");
         break;
	case 5:
		printf("星期五\n");
         break;
	case 6:
		printf("星期六\n");
         break;
	case 7:
		printf("星期天\n");
         break;
	}
   
	return 0;
}
```

![image-20201123211047909](https://i.loli.net/2020/11/29/V9THD48ldSszfQb.png)

相同的case没必要每个后面都加上case,合到一起即可

switch语句中可以出现if

```c
//如果输入了1~7以外的数
//顺序没有严格的措施
default:
	 pritnf("输入错误\n")；
     break;

```

### 练习

```c
#include <stdio.h>
int main()
{
	int n = 1;
	int m = 2;
	switch (n)
	{
	case 1:
		m++;
	case 2:
		n++;
	case 3:
			switch (n)
		{
			case 1:
			n++;
			case 2:
			m++;
			n++;
			break;
		}
	case 4:
		m++;
	default:
		break;
	}
	printf("m = %d\n, n = %d\n", m,  n);
	return 0;
}
//5,3
```



## （2）循环语句

## while

条件表达式的执行次数总是比循环体的执行次数多一次

### 将大写字母转换为小写字母

```c
#include<stdio.h>
int main(void)
{	
    in ch = 0;
    
    while ((ch = getchar()) != EOF)
    {
       printf("%c\n",ch +32);
        getchar();//清理\n
  
    }
    return 0;
}
```



### break:

在循环语句中只要遇到break，就停止后期所有的循环，直接终止循环。所以:while中的break是用于永久终止循环的。

### continue：

是用于终止本次循环的，也就是本次循环中continue后边的代码不会再执行，而是直接跳转到while语句的判断部分。进行下一循环的入口判断。

```c

//条件为真就执行 循环执行 直至为假
#include <stdio.h>
int main()
{	
    while (1)
        printf("hehe\n");
    return 0;
}
```

```c
#include <stdio.h>
int main()
{	
    int i =1;
    while(i<=10)
    {	
        if(i == 5)
            //break;
            //1234
       //达到满足条件循环停止
            //continue;
            //1234 
        //回到while 陷入死循环
        printf("%d",i);
        i++;
    }
    return 0;
}
```



### getchar putchar

可以接受一个键盘的字符

```c
#include <stdio.h>
int main()
{	
    int ch = 0;
    //getchar遇到 CTRL+Z停止
    //EOF文件结束标志
    while((ch = getchar()) != EOF)
    {
        putchar(ch);
    }
    
    printf("%c\n",ch);
    return 0;
}
```



### 小练习

```c
//出错了
#include <stdio.h>
int main()
{	
    int ret =0;
    //接收
    char password [20] = {0};
    printf("请输入密码：>");
    scanf("%s",password);
    //输入密码 并存放在password数组中
    printf("请确认Y/N：>");
    ret = getchar();
    if(ret == 'Y')
    {
        printf("确认成功\n")；
    }
    else
    {
        printf("放弃确认\n")；
    }
    return 0;
}
```



## for（一小部分文件丢失——暂未补充）

语法for（exp1；exp2；exp3）

​				循环语句

exp1为初始化部分，用于初始化循环变量

exp2为条件判断部分，判断条件是否终止

exp3为循环调整，

把while循环中的三个部分放到了一起

```c
#include <stdio.h>
int main()
{	
    int i = 0;
    for(i = 1;i<=10;i++)
    {
        printf("%d",i);
    }
    return 0;
}
```



### for 语句的循环控制变量

(1)不可以在for循环体内修改循环变量，防止for循环失去控制，陷入死循环

![image-20201126201152688](https://i.loli.net/2020/11/29/fVl9D6ogEyKJscb.png)

（2）建议for语句的循环控制变量的取值采用“前闭后开区间”写法

for循环的初始化判断调整都是可以省略的

for(;;) 但是for循环的判断条件被省略的话，判断条件就恒为真，陷入死循环

如果不熟练，建议不要随便省略

```c
#include <stdio.h>
int main(void)
{	
    for(;;)
    {
        printf("HEHE")；
    }
    return 0;	
    
}
```



```c
#include <stdio.h>
int main(void)
{	
     int  i = 0;
     int j = 0;
    for(;i<10;i++)
    {
        for(;j<10;j++)
        {
            printf("hehe\n")
    return 0;
}
        
       //输出运行结果为十个hehe
```



执行流程

![image-20201126172057875](https://i.loli.net/2020/11/29/sKzcgl35FmDwC9v.png)

## do while

do语句

循环语句

```c
#include <stdio.h>
{
int i = 1;
do
{
    printf("%d",i);
    i++;
}
while(i<=10);
return 0;
}
```



do语句的特点：

循环至少执行一次，使用的场景有限，所以不是经常使用



# 27.获取/显示字符

## 缓冲区:输入的东西会先被存放在缓冲区

输入缓冲区在键盘和getchar中间

## （1）getchar

在这里

在代码运行窗口输入的回车也会被当做字符而被获取

例如

```c
//这里输入一个1 结果：c1= 1 ,c2 = ,（\n）回车（换行符）
#include <stdio.h>
int main()
{
    char c1,c2;
    printf("请输入一个数字:\n");
    c1 =  getchar();
    c2 =  getchar();
    printf("c1 = %c.c2 = %c\n"，c1,c2);
    return 0;
}
```



## (2)putchar

```c
#include<stdio.h>
 int main()
 {	
     int ch = 0;
     while(ch = (getchar)) ! = EOF)
     {
         if (ch < '0' || ch > '9')
             continue;
         putchar(ch);
     }
     return 0;
 }
```



# 28.scanf与scanf_s

scanf_s是scanf的安全版本

# 29.综合练习（2）

## 1.计算n的阶乘

```c
//不考虑溢出的情况
#include<stdio.h>
int main(void)
{	
    int i = 0;
    int n = 0;
    int ret = 1;
    printf("请输入一个整数：");
   	scanf("%d",&n);
    for(i = 1;i <= n ;i++)
    {
        ret = ret * i;
    }
    printf("ret = %d",ret);
    return 0;
}
```

## 2.计算1~10的阶乘

在1题的基础上，在外面再套一个循环

```c
#include<stdio.h>
int main(void)
{	
    int i = 0;
    int n = 0;
    int ret = 1;
    int sum = 0;
    printf("请输入10：");
   	scanf("%d",&n);
   for(n =1 ;n<=10;n++)
   {
    for(i = 1;i <= n ;i++)
    	{
        ret = ret * i;
    	}
       sum = sum + ret;
   }
    printf("sum = %d",sum);
    return 0;
}
```

## 3.在一个有序数组在这中查找具体的某个数字n

```c
#include <stdio.h>
int main(void)
{	
    int arr[] = {1,2,3,4,5,6,7,8,9,10};
    int k = 7;
    //写一个代码，在arr数组中找到7
    int  i = 0;
    int sz = sizeof(arr)/sizeof(arr[0]);
    for(i = 0;i <sz;i++);
    {
        if(k == arr[i])
        {
            printf("找到了，下标是:%d\n",i);
            break;
        }
    }
    if (k == sz)
    {
        printf("找不到\n");
    }
    return 0;
}
```



若有n个元素，最坏的情况要找n次，

或者先找中间元素，逐渐缩小查找范围

## 4.编写代码掩饰多个字符从两端移动，向中间汇聚。

## 5.编写模拟登陆场景

并且只能登陆三次，只允许输入三次密码，如果密码正确则提示登录成功，如果三次均输入错误，则退出程序。

```c
#include <stdio.h>
int main(void)
{	
    
    return 0;
}
```

## 6.写三个数让他从大到小输出

```c
#include<stdio.h>
int main(void)
{	
    int a = 0;
    int b = 0;
    int c = 0;
    printf(" 请输入三个数：");
    scanf("%d%d%d",&a,&b,&c);
    //算法实现 
    //a中放最大值 b次之 C中放最小值
    if(a<b)
    {
        //临时变量 防止在将a的值赋给b时，a的值丢失
        int tmp = a; 
        a = b;
        b = tem;
    }
    if(a<c)
    {
        int tmp = a;
        a = c ;
        c = tmp;
    }
    printf("%d %d %d\n", a, b, c);
	return 0;
}
```



7.打印1~100所有3的倍数 

```c
#include<stdio.h>
int main(void)
{
    int i = 0;
    for(int i = 1;i <= 100;i++)
    {
        if(i%3 == 0)
        {
            printf("%d",i);
        }
    }
    return 0;
}
```

## 8.给定两个数求最大公约数

辗转相除法

```c
#include<stdio.h>
int main(void)
{	
	int n,r,m;
	printf("请输入两个大于零的整数：");
	scanf("%d %d", &m, &n);
	while (r = m%n)
	{
		m = n;
		n = r;
	}
	printf("%d\n", n);
	return 0;
}
```



## 9.打印1000~2000的闰年

1判断闰年的方法，能被4整除且不能被100整除

2能被400整除是闰年

```c
#include<stdio.h>
int main(void)
{
	int year;
	for (int year = 1000; year <= 2000; year++)
	{
		if (year % 4 == 0 && year % 100 != 0)
		{
			printf("%d\n", year);
		}
		else if (year % 400 == 0)
		{
			printf("%d\n", year);
		}
	}
	return 0;
}
```



![image-20201203160113405](2020.12.2.assets/image-20201203160113405.png)





## 10.打印100~200的所有素数

​		

```c
//试除法
#include<stdio.h>
int main(void)
{
    //定义整型并初始化
	int num = 0;
	int i = 0;
	int count = 0;

        //理解
        //让i从2开始
        //num开始被i除一直除到num-1
        //如果其中有num被i整除了，循环就终止，break
        //因为素数是除了1和他本身之外不能被其他数所整除
        //在这里只要i和Num不相等，num被其他说所整除，说明，num不是个属于素数，什么也不输出，1是默认的，可以将任意的num整除，在这里i从2开始，所以是素数的数只能被其本身所整除，即i = num,
    	for (int num = 100; num <= 200; num++)
	{
            //i <num
            //筛选——输出
            //有其他可以将num整除的数也将终止循环
		for (  i = 2; i < num; i++)
		{
			if (num%i == 0)
			{
				break;
			}
		}
            //跳出来
            //就是i把num-1的数字都试过了，都不能将num整除
            //那么就是剩下i = num的情况了，此时这个数肯定为素数
            // i  =num
		if (num == i)
		{
			printf("%d\n", num);
			count++;
		}
	}
	printf("一共有%d个质数\n", count);
	return 0;
}



///////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////


//如果一个数可以写成a*b的形式，例如；c = a*b
//那么a和b中的一个数肯定小于等于开平方c
//sqrt——开平方的数学库函数—#include<math.h>
//方法2
```





## 11.求十个数的最大值

```c
#include <stdio.h>
int main(void)
{	
    int arr[] = {1,2,3,4,5,6,7,8,9,10};
    int max = 0;//最大值
    int i = 0;
    int sz = sizeof(arr)/sizeof(arr[0]);
    for(i = 0;- < sz;i++)
    {
        if(arr[i] > max)
        {
            max = arr[i]; 
        }
    }
    printf("max = %d\n"
ma*
           .0
        
      
        
    return 0;
}
```





## 12.求1+。。。。1/10的和









## 13.乘法口诀标的打印

```c
//在屏幕上输出乘法口诀表
//分析：9行，多上行行号就是多少，先确定行，再确定列
#include <stdio.h>
int main(void)
{
	int i = 0;//行
	//确定打印9行
	for (i = 1; i <= 9; i++)//循环-行数
	{
		//每行打印的东西
		int j = 1;
		for (j = 1; j <= i; j++)
		{
            //%2d-打印两位 如果没有两位就用空格补齐
            //%-2d  向左对齐
			printf(" %d * %d = %-2d"i, j, i*j);

		}
		printf("\n");
	}
	return 0;
}
```



## 14.二分查找

```c
#include<stdio.h>
int main(void)
{	
    
    return 0;
}
```



## 15.猜数字游戏

1程序运行 电脑随机生成一个数字 猜大猜小有提示 

2菜单 循环运行

```c
#include<stdio.h>
int main(void)
{	
    
    return 0;
}
```





# 





+